lint:
	pipenv run flake8

test:
	python -m unittest -v

encrypt:
	pipenv run python -m cipher encrypt 'hello, world!'

decrypt:
	pipenv run python -m cipher decrypt 'KHOOR, ZRUOG!'

key:
	pipenv run python -m cipher key

tree:
	tree  -aI '.git|__pycache__'