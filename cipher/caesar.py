import string


class Caesar():
    def __init__(self, offset=3):
        self.__offset = offset

    def encrypt(self, plaintext):
        uptext = plaintext.upper()
        ciphertext = ''

        for character in uptext:
            letter_count = len(string.ascii_uppercase)
            char_index = string.ascii_uppercase.find(character)
            if char_index == -1:
                ciphertext += character
            else:
                index = (char_index + self.__offset) % letter_count
                ciphertext += string.ascii_uppercase[index]

        return ciphertext

    def decrypt(self, ciphertext):
        uptext = ciphertext.upper()
        plaintext = ''

        for character in uptext:
            letter_count = len(string.ascii_uppercase)
            char_index = string.ascii_uppercase.find(character)
            if char_index == -1:
                plaintext += character
            else:
                index = (char_index - self.__offset) % letter_count
                plaintext += string.ascii_uppercase[index]

        return plaintext
